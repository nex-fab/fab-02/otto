1. 画圆
![](https://gitlab.com/ottopicbed/bed2/uploads/b0cee27e6e5bb3d7d4c5945623526f8f/90fedf3aa16ef151386f902ca3af5e4.jpg)
2. 做半圆，然后实体等距封闭半圆
![](https://gitlab.com/ottopicbed/bed2/uploads/fac4489e0438a805177c0a7729b01134/0fac47d24082ad30a57e6a8a8f9d761.jpg)
3. 旋转形成空心球
   ![](https://gitlab.com/ottopicbed/bed2/uploads/cc78c663c2c3ede0392c4229ab8d2e0a/0d87907ea4d43a8d9c2fcb9090a0afe.jpg)
4. 新建 基准面
![](https://gitlab.com/ottopicbed/bed2/uploads/20e56f78305d8ea6eb7c72a8b985d91b/f826170374f71a8ccc15e9d826df5d3.jpg)
5. 剪裁多余实体!!!
![](https://gitlab.com/ottopicbed/bed2/uploads/6db7b0c16293f7811d1399a3af77556d/1e19115c2ac41defc1e1965b664d7ed.jpg)
6. 得到开口中空球后，新建组装体 
7. 开口实体引用，得到底座， 反向得到内扣.
![](https://gitlab.com/ottopicbed/bed2/uploads/0da1fc16106c1b2fad7cda0d723b5b3f/652bcf797efd5921e78019aa5a4c3f8.jpg)
8. 底座处画小圆拉伸剪裁得到电池空间
![](https://gitlab.com/ottopicbed/bed2/uploads/42ec1f4f1ca02cd57329029cebdc6473/06bf3968ca39029a332401484b3c0a5.jpg)
9. 内扣顶部画更小圆，裁出走线空间
![](https://gitlab.com/ottopicbed/bed2/uploads/8e0aa82f41c3e183ec1ce904e34b125d/f08b00aa8899c80da0effbe1f1ab7ab.jpg)

